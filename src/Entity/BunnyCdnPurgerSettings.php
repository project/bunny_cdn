<?php

namespace Drupal\bunny_cdn\Entity;

use Drupal\purge\Plugin\Purge\Purger\PurgerSettingsBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerSettingsInterface;

/**
 * Defines the Bunny CDN purger settings entity.
 *
 * @ConfigEntityType(
 *   id = "bunny_cdn_purger_settings",
 *   label = @Translation("Bunny CDN purger settings"),
 *   config_prefix = "settings",
 *   static_cache = TRUE,
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "api_key",
 *     "pull_zone",
 *     "base_url",
 *   }
 * )
 */
class BunnyCdnPurgerSettings extends PurgerSettingsBase implements PurgerSettingsInterface {

  /**
   * The readable name of this purger.
   */
  public string $name = '';

  /**
   * The API key.
   */
  public string $api_key = '';

  /**
   * The pull zone ID.
   */
  public string $pull_zone = '';

  /**
   * The base URL of this website.
   *
   * Should contain the scheme, hostname and an optional path prefix.
   * Will be used to build the full URL for path-based purges.
   */
  public string $base_url = '';

}
