<?php

namespace Drupal\bunny_cdn\EventSubscriber;

use Drupal\Core\Cache\CacheableResponseInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Generates a 'CDN-Tag' header in the format expected by BunnyCDN.
 */
class CdnTagResponseHeaderSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

  /**
   * Adds a 'CDN-Tag' header to the response.
   */
  public function onRespond(ResponseEvent $event): void {
    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();
    if (!$response instanceof CacheableResponseInterface) {
      return;
    }

    $cacheMetadata = $response->getCacheableMetadata();
    $cacheTags = $cacheMetadata->getCacheTags();

    if ($cacheTags === []) {
      return;
    }

    // BunnyCDN stores a single tag for a request. To be able to purge those
    // using "* CACHETAG *", add spaces before/between/after each cache tag.
    // Add trailing/leading colons just in case spaces would get trimmed.
    $response->headers->set('CDN-Tag', ': ' . implode(' ', $cacheTags) . ' :');
  }

}
