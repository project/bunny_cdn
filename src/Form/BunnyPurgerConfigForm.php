<?php

namespace Drupal\bunny_cdn\Form;

use Drupal\bunny_cdn\Entity\BunnyCdnPurgerSettings;
use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring the Bunny CDN purger.
 */
class BunnyPurgerConfigForm extends PurgerConfigFormBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->httpClient = $container->get('http_client');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bunny_cdn.purger_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array $options = []): array {
    $config = BunnyCdnPurgerSettings::load($this->getId($form_state));

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('A label that describes this purger.'),
      '#default_value' => $config->name,
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->api_key,
      '#required' => TRUE,
    ];

    $form['pull_zone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pull Zone ID'),
      '#default_value' => $config->pull_zone,
      '#required' => TRUE,
    ];

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#description' => $this->t('The base URL of this website containing the scheme, hostname and an optional path prefix. Will be used to build the full URL for path-based purges.'),
      '#default_value' => $config->base_url,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $apiKey = $form_state->getValue('api_key');
    $pullZoneId = $form_state->getValue('pull_zone');

    if (!is_numeric($pullZoneId)) {
      $form_state->setErrorByName('pull_zone', $this->t('The Pull Zone with the requested ID does not exist.'));
      return;
    }

    try {
      $url = sprintf('https://api.bunny.net/pullzone/%s', $pullZoneId);
      $this->httpClient->request('GET', $url, [
        RequestOptions::HEADERS => [
          'AccessKey' => $apiKey,
        ],
      ]);
    }
    catch (GuzzleException $exception) {
      if ($exception instanceof BadResponseException) {
        $response = $exception->getResponse();
        if ($response->getStatusCode() === 401) {
          $form_state->setErrorByName('api_key', $this->t('The API key is invalid.'));
          return;
        }
        if ($response->getStatusCode() === 404) {
          $form_state->setErrorByName('pull_zone', $this->t('The Pull Zone with the requested ID does not exist.'));
          return;
        }
      }

      $form_state->setErrorByName('api_key', $this->t('Bunny API request failed. Status code %code: %message', [
        '%code' => $exception->getCode(),
        '%message' => $exception->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state, array $options = []): void {
    $config = BunnyCdnPurgerSettings::load($this->getId($form_state));
    $config->set('name', $form_state->getValue('name'));
    $config->set('api_key', $form_state->getValue('api_key'));
    $config->set('pull_zone', $form_state->getValue('pull_zone'));
    $config->set('base_url', rtrim($form_state->getValue('base_url'), '/'));
    $config->save();
  }

}
