<?php

namespace Drupal\bunny_cdn\Plugin\Purge\Purger;

use Drupal\bunny_cdn\Entity\BunnyCdnPurgerSettings;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvStatesInterface;
use Drupal\purge\Plugin\Purge\Invalidation\PathInvalidation;
use Drupal\purge\Plugin\Purge\Invalidation\TagInvalidation;
use Drupal\purge\Plugin\Purge\Invalidation\UrlInvalidation;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Purger implementation for bunny.net.
 *
 * @PurgePurger(
 *   id = "purge_purger_bunny",
 *   label = @Translation("Bunny CDN"),
 *   configform = "\Drupal\bunny_cdn\Form\BunnyPurgerConfigForm",
 *   cooldown_time = 1.0,
 *   description = @Translation("Purger for Bunny CDN."),
 *   multi_instance = TRUE,
 *   types = {
 *     "tag",
 *     "url",
 *     "wildcardurl",
 *     "path",
 *     "wildcardpath",
 *     "everything",
 *   },
 * )
 */
class BunnyPurger extends PurgerBase {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    $this->entityTypeManager->getStorage('bunny_cdn_purger_settings')
      ->delete([$this->getPurgerSettings()]);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    throw new \LogicException('You should not be here.');
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type): string {
    return match ($type) {
      'tag' => 'invalidateTags',
      'url', 'wildcardurl', 'path', 'wildcardpath' => 'invalidateUrls',
      'everything' => 'invalidateAll',
      default => 'invalidate',
    };
  }

  /**
   * Invalidate a set of urls.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateUrls(array $invalidations): void {
    $config = $this->getPurgerSettings();

    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvStatesInterface::PROCESSING);

      if ($invalidation instanceof UrlInvalidation) {
        $urlToInvalidate = $invalidation->getUrl()->setAbsolute()->toString();
      }

      if ($invalidation instanceof PathInvalidation) {
        $urlToInvalidate = $config->base_url . '/' . $invalidation->getExpression();
      }

      try {
        $url = 'https://api.bunny.net/purge';
        $this->httpClient->request('POST', $url, [
          RequestOptions::QUERY => [
            'url' => $urlToInvalidate,
          ],
          RequestOptions::HEADERS => [
            'AccessKey' => $config->api_key,
          ],
        ]);
      }
      catch (GuzzleException $e) {
        $this->logger()->error('Bunny purge request failed. Status code %code: %message',
              ['%code' => $e->getCode(), '%message' => $e->getMessage()]);
        $invalidation->setState(InvStatesInterface::FAILED);

        continue;
      }

      $invalidation->setState(InvStatesInterface::SUCCEEDED);
    }
  }

  /**
   * Invalidate a set of tags.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   */
  public function invalidateTags(array $invalidations): void {
    $config = $this->getPurgerSettings();

    foreach ($invalidations as $invalidation) {
      assert($invalidation instanceof TagInvalidation);
      $invalidation->setState(InvStatesInterface::PROCESSING);

      try {
        $url = sprintf('https://api.bunny.net/pullzone/%s/purgeCache', $config->pull_zone);
        $this->httpClient->request('POST', $url, [
          RequestOptions::JSON => [
            'CacheTag' => sprintf('* %s *', $invalidation->getExpression()),
          ],
          RequestOptions::HEADERS => [
            'AccessKey' => $config->api_key,
          ],
        ]);
      }
      catch (GuzzleException $e) {
        $this->logger()->error(
          'Bunny purge request failed. Status code %code: %message',
          ['%code' => $e->getCode(), '%message' => $e->getMessage()],
        );
        $invalidation->setState(InvStatesInterface::FAILED);

        continue;
      }

      $invalidation->setState(InvStatesInterface::SUCCEEDED);
    }
  }

  /**
   * Invalidate everything.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   */
  public function invalidateAll(array $invalidations): void {
    $config = $this->getPurgerSettings();
    $this->updateState($invalidations, InvStatesInterface::PROCESSING);

    try {
      $url = sprintf('https://api.bunny.net/pullzone/%s/purgeCache', $config->pull_zone);
      $this->httpClient->request('POST', $url, [
        RequestOptions::HEADERS => [
          'AccessKey' => $config->api_key,
        ],
      ]);
    }
    catch (GuzzleException $e) {
      $this->logger()->error(
        'Bunny purge request failed. Status code %code: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()],
      );
      $this->updateState($invalidations, InvStatesInterface::FAILED);

      return;
    }

    $this->updateState($invalidations, InvStatesInterface::SUCCEEDED);
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement(): bool {
    return TRUE;
  }

  /**
   * Update the invalidation state of items.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   * @param int $invalidation_state
   *   The invalidation state.
   */
  protected function updateState(array $invalidations, int $invalidation_state): void {
    // Update the state.
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($invalidation_state);
    }
  }

  /**
   * Converts any path or URL into a normalized path.
   *
   * @param string $url
   *   URL to normalize.
   *
   * @return string
   *   Returns normalized path.
   */
  public function normalizePath(string $url): string {
    $parsed_url = parse_url($url);
    $path = $parsed_url['path'] ?? '';
    $query = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';

    return $path . $query;
  }

  /**
   * Returns settings for this purger.
   *
   * The return type is defined loosely for the benefit of child classes that
   * may wish to override the source for configuration.
   */
  protected function getPurgerSettings(): object {
    return BunnyCdnPurgerSettings::load($this->id);
  }

}
